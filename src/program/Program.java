package program;

import dao.DAOaccounts;
import service.Account;
import service.Serializer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Program {


  private static Account[] accounts = new Account[] {
      new Account(7765, "Dave", 468500L),
      new Account(6534, "Anna", 433200L), 
      new Account(2365, "Lex", 8768500L),
      new Account(98765, "Josh", 9800L), 
      new Account(65, "Dave", 0L)};

  private static String filename = "src/accounts/";

  public static void main(String[] args) {
//     writeAccounts(accounts);
//     printAccounts();

    Thread process1 = new Thread(new Runnable() {
      @Override
      public void run() {
        DAOaccounts.transfer(filename + accounts[0].getId(), filename + accounts[1].getId(), 7L);
      }
    });

    Thread process2 = new Thread(new Runnable() {
      @Override
      public void run() {
        DAOaccounts.transfer(filename + accounts[1].getId(), filename + accounts[2].getId(), 7L);
      }
    });

    Thread process3 = new Thread(new Runnable() {
      @Override
      public void run() {
        DAOaccounts.transfer(filename + accounts[2].getId(), filename + accounts[4].getId(), 7L);
      }
    });

    ExecutorService executorService = Executors.newFixedThreadPool(33);

    for (int i = 0; i < 33; i++) {
      executorService.execute(process1);
      executorService.execute(process2);
      executorService.execute(process3);
    }
   
    printAccounts();
  }

  private synchronized static void writeAccounts(Account[] accounts) {
    for (int i = 0; i < accounts.length; i++) {
      Serializer.serializeAccount(filename + accounts[i].getId(), accounts[i]);
    }
  }

  private synchronized static void printAccounts() {
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e1) {
      e1.printStackTrace();
    }

    try {
      Account deserizal;
      StringBuilder stringBuilder = new StringBuilder().append("\n");
      long sum = 0;

      for (int i = 0; i < accounts.length; i++) {
        deserizal = Serializer.deserializeAccount(filename + accounts[i].getId());
        sum += deserizal.getBalance();
        stringBuilder.append("id: ").append(deserizal.getId()).append(", name: ")
            .append(deserizal.getName()).append(", balance: ").append(deserizal.getBalance())
            .append("\n");
      }

      stringBuilder.append("total balance: ").append(sum).append("\n");
      System.out.println(stringBuilder);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

}
