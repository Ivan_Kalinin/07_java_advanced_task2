package dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import exceptions.ImpossibleTransferException;
import service.Account;
import service.Serializer;

public class DAOaccounts {
  private static final Logger log = LogManager.getLogger(DAOaccounts.class);

  public synchronized static void transfer(String fileAccountFrom, String fileAccountTo,
      Long amount) {
    long startTime = System.currentTimeMillis();

    log.info(fileAccountFrom + " - " + fileAccountTo + " transfer started");
    try {
      Account accountFrom = Serializer.deserializeAccount(fileAccountFrom);
      Account accountTo = Serializer.deserializeAccount(fileAccountTo);
      accountFrom.transferBalance(accountTo, amount);

      Serializer.serializeAccount(fileAccountFrom, accountFrom);
      Serializer.serializeAccount(fileAccountTo, accountTo);

    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (ImpossibleTransferException e) {
      e.printStackTrace();
    }

    long estimatedTime = System.currentTimeMillis() - startTime;
    log.info(fileAccountFrom + " - " + fileAccountTo + " transfer completed. duration (sec): "
        + estimatedTime * 0.001);
  }
}
