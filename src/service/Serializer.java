package service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Serializer {
  private static final Logger log = LogManager.getLogger(Serializer.class);

  static public synchronized Account deserializeAccount(String filename)
      throws ClassNotFoundException {
    Account account = null;

    try (FileInputStream fileInputStream = new FileInputStream(filename);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
      account = (Account) objectInputStream.readObject();
      log.info(filename + " - deserialized");
    } catch (IOException e) {
      e.printStackTrace();
    }

    return account;
  }

  static public synchronized void serializeAccount(String filename, Account account) {
    try (FileOutputStream fileOutputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {

      objectOutputStream.writeObject(account);
      log.info(filename + " - serialized");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
