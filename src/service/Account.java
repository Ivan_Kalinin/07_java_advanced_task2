package service;

import java.io.Serializable;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import dao.DAOaccounts;
import exceptions.ImpossibleTransferException;
import exceptions.NegativeBalanceException;

public class Account implements Serializable, Comparable<Account> {
  private static final Logger log = LogManager.getLogger(DAOaccounts.class);
  private static final long serialVersionUID = 7326594746803270302L;
  private volatile int id;
  private volatile String name;
  private volatile Long balance;

  public Account(int id, String name, Long balance) {
    super();
    this.id = id;
    this.name = name;
    this.balance = balance;
  }

  public int getId() {
    return id;
  }

  private void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  private void setName(String name) {
    this.name = name;
  }

  public Long getBalance() {
    return balance;
  }

  private void setBalance(Long balance) throws NegativeBalanceException {
    if (balance > 0)
      this.balance = balance;
    else
      throw new NegativeBalanceException();
  }

  public void transferBalance(Account accountTo, Long amount)
      throws ImpossibleTransferException {
    if (this.compareTo(accountTo) == 0) {
      log.warn("self-transfer " + this.getId());
      return;
    }
    try {
      this.setBalance(this.getBalance() - amount);
      accountTo.setBalance(accountTo.getBalance() + amount);
    } catch (NegativeBalanceException e) {
      e.printStackTrace();
      throw new ImpossibleTransferException();
    }

  }

  @Override
  public int compareTo(Account account) {
    if (this.getId() == account.getId())
      return 0;
    else if (this.getId() > account.getId())
      return 1;
    else
      return -1;
  }
}
