package exceptions;

public class ImpossibleTransferException extends Exception {

  @Override
  public String getMessage() {
    return "Transfer operation is impossible";
  }

  private static final long serialVersionUID = 148241898111549961L;

}
