package exceptions;

public class NegativeBalanceException extends Exception {

  @Override
  public String getMessage() {
    return "Balance sould not be negative";
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
